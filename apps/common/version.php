<?php
return array(
    // 应用版本
    'app_version' => '3.1.1',
    
    // 发布时间
    'release_time' => '20211026',
    
    // 修订版本
    'revise_version' => '4'

);
